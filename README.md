# Accessible Google Maps shim

WIP collaboration to make a keyboard- and screen reader-accessible shim for the Google Maps API.

## Requirements
The shim currently requires jQuery (referenced as 'jQuery,' most versions should work fine). You can probably modify pretty easily to work with your library of choice.

## Installation
To add the shim to your project:
1. Load accessible_gmaps.js and accessible_gmaps.css into any page containing a Google map. Remember that jQuery is also required.  
2. When creating markers or adding new markers to the map, call `accessible_gmaps.addMarker(marker)`, where `marker` is the gMaps marker object.  
	*Each marker object should have a title property attached. This string is used by base gMaps to provide title text for markers, and is used by the accessibility shim to provide information about the marker to screen readers. Set this property with `marker.title='your title here'`*  
3. After your map is loaded and has bounds (usually just before you'd display it to the user), call `accessible_gmaps.onload(map, mapIdentifier, options)` with these parameters:  
    + `map`: the map object (created with new google.maps.Map())  
    + `mapIdentifier`: a CSS identifier unique to your map, used by jQuery to add the shim elements.  
    + `options`: an array of settings for the shim. Current options are:  
		+ `infowindows`: if set to TRUE, will create keyboard-accessible infowindows attached to map markers.  
		*This option expects each marker object to have a windowContent property, which contains an HTML string to use as the infowindow content (minus the window header, which is automatically generated from the marker's title attribute). Set this property with `marker.windowContent='your content here'`*  
		
		
## Known issues:
+ The shim has some serious performance impact on maps with lots of markers. Repositioning all of the overlay divs is time-intensive - this needs to be optimized.
+ The marker overlays have some drawbacks over traditional mouse navigation:  
    + They capture scroll wheel input to scroll the whole page, rather than zoom the map, when they're in hover.
    + It can be very difficult to select markers that are hidden behind other markers.
    + They aren't dynamically sized to the marker's size, so they're unwieldy if your markers are nonstandard.
+ There's no way to add new markers to the map after the onload function is called.
+ You can only remove markers by setting them as invisible; actually removing them from the map will break the shim.
+ Any infowindows that exist on the page other than the one created by this map (including in other maps on the page) might break the map.
+ There's no way to have two accessible maps on the same page.
