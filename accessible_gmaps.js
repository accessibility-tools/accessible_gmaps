(function(window){
    'use strict';

	function load_accessible_gmaps(){
		var accessible_gmaps = {};
		
		accessible_gmaps.markers = [];
		accessible_gmaps.infowindowsEnabled;
		accessible_gmaps.map;
		accessible_gmaps.infowindow;
		accessible_gmaps.mapIdentifier;
		accessible_gmaps.initialCenter;
		accessible_gmaps.keys = { // Define values for keycodes
			 backspace:  8,
			 tab:        9,
			 enter:      13,
			 shift:      16, // defined for keyUp event handler - firefox browser fix
			 ctrl:       17, // defined for keyUp event handler - firefox browser fix
			 alt:        18, // defined for keyUp event handler - firefox browser fix
			 esc:        27,

			 space:      32,
			 pageup:     33,
			 pagedown:   34,
			 end:        35,
			 home:       36,

			 left:       37,
			 up:         38,
			 right:      39,
			 down:       40,

			 del:        46
		   };

		/**
		 *	Call this function after all markers have been added using addMarker().
		 *	
		 *	This master function builds the accessible map shim by:
		 *		- adding keyboard-accessible accessible map controls
		 *		- creating and positioning marker overlays (for keyboard tabbing through markers)
		 *		- (if enabled) creating a keyboard-accessible modal infowindow
		 *		- adding listeners necessary to keep created objects up-to-date
		 *	
		 *	I might change this function so that marker divs are created by addMarker(),
		 *	and just positioned in this function. I'm not sure which use case is more viable.
		 */
		accessible_gmaps.onload = function(map, mapIdentifier, options){
			
			console.log("Accessibility shim loaded!");
			
			// Collect data for shim global variables
			accessible_gmaps.mapIdentifier = mapIdentifier;
			accessible_gmaps.map = map;
			accessible_gmaps.initialCenter = accessible_gmaps.map.getCenter();
			if(options.infowindows){
				accessible_gmaps.infowindowsEnabled = true;
				console.log("Infowindows enabled!");
			}
			
			
			// Add listeners to reposition marker overlays
			map.addListener('center_changed', function() {
				accessible_gmaps.resetAllMarkers();
			});
			map.addListener('zoom_changed', function() {
				accessible_gmaps.resetAllMarkers();
			});	
			
			
			// Create controls and other objects necessary for shim
			accessible_gmaps.init_controls(map);
			if(accessible_gmaps.infowindowsEnabled){
				accessible_gmaps.infowindow = new google.maps.InfoWindow();
			}
			// Create marker overlays
			// We need a timeout - I'm not sure why. But the map bounds aren't caught up with us if we don't wait, and we need the map bounds set before we can position markers.
			setTimeout(function(){
				for(var i = accessible_gmaps.markers.length - 1; i >= 0; i--){
					var marker = accessible_gmaps.markers[i];
					accessible_gmaps.create_marker_div(marker, i);
				}
				accessible_gmaps.resetAllMarkers();
			}, 2000);
		}

		/**
		 *	Adds keyboard-accessible map controls to the map,
		 *	and binds handlers to them for actual map controlling.
		 *	Most of the logic here is Keith's, but it's been modified pretty heavily.
		 */
		accessible_gmaps.init_controls = function(map){
			
			// Markup for keyboard-accessible map controls - there might be better ways to create this, but this is what we have.
			var markup = '<div id="map-controls">';
			markup += '<div id="pan-base">';
			markup += '<div class="accessible_button_wrapper pan-up"><button id="pan-up" class="map-bn">Up</button></div>';
			markup += '<div class="accessible_button_wrapper pan-lt"><button id="pan-lt" class="map-bn">Left</button></div>';
			markup += '<div class="accessible_button_wrapper pan-ctr"><button id="pan-ctr" class="map-bn">Center</button></div>';
			markup += '<div class="accessible_button_wrapper pan-rt"><button id="pan-rt" class="map-bn">Right</button></div>';
			markup += '<div class="accessible_button_wrapper pan-dn"><button id="pan-dn" class="map-bn">Down</button></div>';
			markup += '</div>';
			markup += '<div id="zoom-base">';
			markup += '<div class="accessible_button_wrapper zoom-in"><button id="zoom-in" class="map-bn">Zoom In</button></div>';
			markup += '<div class="accessible_button_wrapper zoom-out"><button id="zoom-out" class="map-bn">Zoom Out</button></div>';
			markup += '</div>';
			markup += '</div>';

			// Add controls to map 
			jQuery(accessible_gmaps.mapIdentifier).append(markup);
			
			// Bind event handlers to controls. These pan values can be adjusted to taste.
			jQuery('button#pan-up').bind('click', function(e) {
				jQuery(this).focus();
				map.panBy(0, -88);
				return false;
			});
			jQuery('button#pan-lt').bind('click', function(e) {
				jQuery(this).focus();
				map.panBy(-112, 0);
				return false;
			});
			jQuery('button#pan-rt').bind('click', function(e) {
				jQuery(this).focus();
				map.panBy(112, 0);
				return false;
			});
			jQuery('button#pan-dn').bind('click', function(e) {
				jQuery(this).focus();
				map.panBy(0, 88);
				return false;
			});
			jQuery('button#pan-ctr').bind('click', function(e) {
				jQuery(this).focus();
				map.setCenter(accessible_gmaps.initialCenter);
				return false;
			});
			jQuery('button#zoom-in').bind('click', function(e) {
				jQuery(this).focus();
				var zoomLevel = map.getZoom();
				map.setZoom(zoomLevel + 1);
				return false;
			});

			jQuery('button#zoom-out').bind('click', function(e) {
				jQuery(this).focus();
				var zoomLevel = map.getZoom();
				map.setZoom(zoomLevel - 1);
				return false;
			});
			jQuery('button.map-bn').bind('mousedown', function(e) {
				jQuery(this).addClass('active');
				return true;
			});
			jQuery('button.map-bn').bind('keydown', function(e) {
				if (e.keyCode == accessible_gmaps.keys.space || e.keyCode == accessible_gmaps.keys.enter) {
					jQuery(this).addClass('active');
				}
				return true;
			});
			jQuery('button.map-bn').bind('mouseup keyup', function(e) {
				jQuery(this).removeClass('active');
				return true;
			});
		}

		/**
		 *	Creates an overlay div for the given map marker.
		 *	This div can receive keyboard focus and allows 
		 *	keyboard users to browse the map with the tab key.
		 */
		accessible_gmaps.create_marker_div = function(marker, markerIndex){
			// Position a focusable div over the maker
			var markerID = "mkr-" + markerIndex;
			var title = marker.getTitle();
			jQuery('div#map-controls').after('<div id="' + markerID + '" title="' + title + '" class="map-marker" tabindex="0"><span>' + title + '</span></div>');	
			jQuery('div#' + markerID).data('markerID', markerIndex) // Data attribute is added to the div so its associated marker can be identified.
			
			// Bind events to the marker hover to reposition its shim:
			marker.addListener('mouseover', function() {
				accessible_gmaps.resetMarker(markerIndex, accessible_gmaps.getMapData());
			});
			
			// Bind events to the marker overlay to manipulate its marker's z index.
			jQuery('div#' + markerID).focus(function(){
				//accessible_gmaps.zUp
				accessible_gmaps.resetMarker(markerIndex, accessible_gmaps.getMapData());
			});
			jQuery('div#' + markerID).blur(function(){
				//accessible_gmaps.zDown
				accessible_gmaps.resetMarker(markerIndex, accessible_gmaps.getMapData());
			});
			
			// If the accessible infowindows module is enabled, bind handlers to open the modal infowindow.
			if(accessible_gmaps.infowindowsEnabled){
				jQuery('div#' + markerID).keyup(function (e) {
					if (e.keyCode == accessible_gmaps.keys.enter) {
						accessible_gmaps.infowindows_openInfoWindow(markerIndex);
					}
				});
				jQuery('div#' + markerID).click(function (e) {
					accessible_gmaps.infowindows_openInfoWindow(markerIndex);
				});
			}
		}

		/**
		 *	Helper function to open the modal infowindow.
		 *	It's assumed this function will only be called
		 *	if the accessible infowindow module is enabled.
		 *	
		 *	TODO: push all marker shims down in z index, so they aren't visible through the infowindow
		 *	TODO: find a better way to grab our infowindow (right now we rely on a class created by Google)
		 */
		accessible_gmaps.infowindows_openInfoWindow = function(markerIndex){
			// Grab map marker; we need its title and windowContent.
			var marker = accessible_gmaps.markers[markerIndex];
			
			var header = "<h2 id='markerHeader-"+markerIndex+"'>"+marker.title+"</h2>";
			// windowContent should have been added to the marker object by the client if they wanted to use the accessible infowindow module.
			accessible_gmaps.infowindow.setContent(header + marker.windowContent);
			accessible_gmaps.infowindow.open(accessible_gmaps.map, marker);
			
			// Grab our infowindow, apply necessary attributes and give it focus
			// We're assuming here that ours is the only infowindow open on the map - it's the only way I can find right now to grab our window for accessibility modifications. 
			var windowWithContent = jQuery('div.gm-style-iw');
			windowWithContent.attr('tabindex', 0);
			windowWithContent.attr('aria-labelledby', "markerHeader-"+markerIndex);
			windowWithContent.focus();
			
			// Remove the default close button and add our own
			var windowCloseButton = windowWithContent.next().children('img');
			windowCloseButton.parent().remove();
			windowWithContent.append("<span role='button' tabindex='0' class='accessibilityInfoClose'>Close infowindow</button>");
			//accessible_gmaps.zUp()
			
			// Attach event handlers to close button.
			jQuery('span.accessibilityInfoClose').keyup(function (e) {
				if (e.keyCode == accessible_gmaps.keys.enter) {
					jQuery('div#mkr-' + markerIndex).focus();
					//accessible_gmaps.zDown()
					windowWithContent.parent().unbind('keydown');
					accessible_gmaps.infowindow.close();
				}
			});
			jQuery('span.accessibilityInfoClose').click(function(e) {
				jQuery('div#mkr-' + markerIndex).focus();
				//accessible_gmaps.zDown()
				windowWithContent.parent().unbind('keydown');
				accessible_gmaps.infowindow.close();
			});
			
			// Attach event handler to infowindow to keep focus inside window (modal-ish dialog)
			windowWithContent.parent().keydown(function (e) {
				accessible_gmaps.infowindows_trapTabKey(windowWithContent.parent(), e, markerIndex);
			});
		}

		/**
		 *	Helper function that traps keyboard focus inside a modal dialog.
		 *	Modified from example at https://accessibility.oit.ncsu.edu/blog/2013/09/13/the-incredible-accessible-modal-dialog/
		 */
		accessible_gmaps.infowindows_trapTabKey = function(object,evt, markerIndex) {
			var focusableElementsString ="a[href], area[href], input:not([disabled]), select:not([disabled]), textarea:not([disabled]), button:not([disabled]), iframe, object, embed, *[tabindex], *[contenteditable]";
			// if tab or shift-tab pressed
			if( evt.which == accessible_gmaps.keys.esc) {
				jQuery('div#mkr-' + markerIndex).focus();
				//accessible_gmaps.zDown()
				object.unbind('keydown');
				accessible_gmaps.infowindow.close();
			}
			else if ( evt.which == accessible_gmaps.keys.tab ) {
				// get list of all children elements in given object
				var o = object.find('*');

				// get list of focusable items
				var focusableItems = o.filter(focusableElementsString).filter(':visible')

				// get currently focused item
				var focusedItem = jQuery(':focus');

				// get the number of focusable items
				var numberOfFocusableItems = focusableItems.length

				// get the index of the currently focused item
				var focusedItemIndex = focusableItems.index(focusedItem);

				//back tab
				if (evt.shiftKey) {
					// if focused on first item and user preses back-tab, go to the last focusable item
					if(focusedItemIndex==0){
						focusableItems.get(numberOfFocusableItems-1).focus();
						evt.preventDefault();
					}
				} 
				//forward tab
				else {
					// if focused on the last item and user preses tab, go to the first focusable item
					if(focusedItemIndex==numberOfFocusableItems-1){
						focusableItems.get(0).focus();
						evt.preventDefault();
					}
				}
			}
		}

		/**
		 *	Repositions marker overlay to sit over the gMaps marker.
		 *	
		 *	TODO: allow offset to be calculated based on marker size data retrieved from user?
		 */
		accessible_gmaps.resetMarker = function(markerIndex, mapData){
			var marker = accessible_gmaps.markers[markerIndex];
			var markerID = "mkr-" + markerIndex;
			
			// If marker isn't visible, hide the div and move on.
			if(!marker.getVisible()){
				jQuery('div#' + markerID).css('display', 'none');
				return;
			}
			
			var offset = accessible_gmaps.getMarkerPos(marker, mapData);

			// These values should be adjusted based on your marker size.
			offset.x = offset.x - 10;
			offset.y = offset.y - 40;
			
			var mapWidth = jQuery(accessible_gmaps.mapIdentifier).width();
			var mapHeight = jQuery(accessible_gmaps.mapIdentifier).height();
			
			var markerWidth = jQuery('div#' + markerID).width();
			var markerHeight = jQuery('div#' + markerID).height();

			if ((offset.x > (0 - markerWidth) && offset.x < mapWidth) && (offset.y > (0 - markerHeight) && offset.y < mapHeight)) {
				jQuery('div#' + markerID).css('display', 'block');
				jQuery('div#' + markerID).css('left', offset.x + 'px');
				jQuery('div#' + markerID).css('top', offset.y + 'px');
			}
			else {
				jQuery('div#' + markerID).css('display', 'none');
			}
		}

		/**
		 *	Moves all marker shims off-screen.
		 */
		accessible_gmaps.resetAllMarkers = function(){
			var mapData = accessible_gmaps.getMapData();
			var bounds = accessible_gmaps.map.getBounds();
			for(var i = 0; i < accessible_gmaps.markers.length; i++){
				var marker = accessible_gmaps.markers[i];
				var markerID = "mkr-" + i;
				// If marker is not in bounds, hide the overlay.
				if(!bounds.contains(marker.getPosition())){
					jQuery('div#' + markerID).css('display', 'none');
				}
				// If the marker is in bounds, position the overlay off-screen. This will keep it from receiving incorrect mouse hover, until it is repositioned by its marker's hover or its own focus event.
				else {
					jQuery('div#' + markerID).css('display', 'block');
					jQuery('div#' + markerID).css('left', '-2000px');
					jQuery('div#' + markerID).css('top', '-2000px');
				}
			}
		}

		/**
		 *	Helper function that modifies the z index of a
		 *	gmaps marker.
		 
		 *	TODO: generalize so this can be called elsewhere
		 */
		accessible_gmaps.zUp = function(){
			var marker = accessible_gmaps.markers[jQuery(this).data('markerID')];
			marker.setZIndex(1000);
		}

		/**
		 *	Helper function that modifies the z index of a
		 *	gmaps marker.
		 
		 *	TODO: generalize so this can be called elsewhere
		 */
		accessible_gmaps.zDown = function(){
			var marker = accessible_gmaps.markers[jQuery(this).data('markerID')];
			marker.setZIndex(1);
		}

		/**
		 *	Function called by client to register a gMaps marker
		 *	with this shim.
		 */
		accessible_gmaps.addMarker = function(marker){
			accessible_gmaps.markers.push(marker);
		}

		/**
		 *	Helper function to calculate the position of a marker overlay.
		 *	Written by Keith and modified for performance.
		 */
		accessible_gmaps.getMarkerPos = function(marker, mapData) {
			var gmap = marker.map;
			
			var worldCoordinate = mapData.projection.fromLatLngToPoint(marker.getPosition());

			var pixelOffset = new google.maps.Point(
				Math.floor((worldCoordinate.x - mapData.worldCoordinateNW.x) * mapData.scale),
				Math.floor((worldCoordinate.y - mapData.worldCoordinateNW.y) * mapData.scale)
			);

			return pixelOffset;
		}

		/**
		 *	Helper function to pull projection information from map.
		 *	This info is needed in getMarkerPos(),
		 *	but can be calculated once for every batch of marker resets.
		 */
		accessible_gmaps.getMapData = function(){
			var mapData = {};
			mapData.scale = Math.pow(2, accessible_gmaps.map.getZoom());
			mapData.nw = new google.maps.LatLng(
				accessible_gmaps.map.getBounds().getNorthEast().lat(),
				accessible_gmaps.map.getBounds().getSouthWest().lng()
			);
			mapData.projection = accessible_gmaps.map.getProjection();
			mapData.worldCoordinateNW = mapData.projection.fromLatLngToPoint(mapData.nw);
			
			return mapData;
		}

	return accessible_gmaps;
	}

	//define globally if it doesn't already exist
	if(typeof(accessible_gmaps) === 'undefined'){
		window.accessible_gmaps = load_accessible_gmaps();
	}
	else{
		console.log("accessible_gmaps already defined.");
	}
})(window);